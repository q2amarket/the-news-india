package com.codepaints.theindiannews;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.loader.app.LoaderManager;
import androidx.loader.content.Loader;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class MainActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<List<News>> {

    public static final  String                     LOG_TAG          = MainActivity.class.getSimpleName();
    private static final int                        NEWS_LOADER_ID   = 1;
    private static       String                     NEWS_REQUEST_URL = "https://content.guardianapis.com/search?tag=world/india&format=json&page-size=100&show-tags=contributor&show-fields=starRating,headline,trailText,thumbnail,publication&api-key=test";
    private              ArrayList<News>            mNews;
    private              RecyclerView               mRecyclerView;
    private              RecyclerView.LayoutManager mLayoutManager;
    private              RecyclerView.Adapter       mAdapter;
    private              TextView                   emptyView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Objects.requireNonNull(getSupportActionBar()).setDisplayShowHomeEnabled(true);
        getSupportActionBar().setLogo(R.drawable.actionbarlogo);
        getSupportActionBar().setDisplayUseLogoEnabled(true);

        mRecyclerView = findViewById(R.id.news_list);
        emptyView = findViewById(R.id.empty_list);

        mNews = new ArrayList<>();

        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(this);
        mAdapter = new NewsAdapter(mNews, this);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setAdapter(mAdapter);



        // connection
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo         networkInfo         = connectivityManager.getActiveNetworkInfo();

        if (networkInfo != null && networkInfo.isConnected()) {
            LoaderManager loaderManager = getSupportLoaderManager();
            loaderManager.initLoader(NEWS_LOADER_ID, null, this);
        } else {
            View loadingIndicator = findViewById(R.id.loading_indicator);
            loadingIndicator.setVisibility(View.GONE);

            emptyView.setText(R.string.no_internet_connection);
        }

    }

    @NonNull
    @Override
    public Loader<List<News>> onCreateLoader(int id, @Nullable Bundle args) {
        return new NewsLoader(this, NEWS_REQUEST_URL);
    }

    @Override
    public void onLoadFinished(@NonNull Loader<List<News>> loader, List<News> data) {

        View loadingIndicator = findViewById(R.id.loading_indicator);
        loadingIndicator.setVisibility(View.GONE);

        if (data.isEmpty()) {
            emptyView.setText(R.string.no_news_found);
        }

        mNews.clear();

        if (data != null && !data.isEmpty()) {
            mNews.addAll(data);
            mAdapter.notifyDataSetChanged();
        }

    }

    @Override
    public void onLoaderReset(@NonNull Loader<List<News>> loader) {
        mNews.clear();
    }
}
