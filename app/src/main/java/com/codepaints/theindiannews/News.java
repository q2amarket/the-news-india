package com.codepaints.theindiannews;

public class News {

    private String mSectionName;
    private String mWebPublicationDate;
    private String mWebUrl;
    private String mHeadline;
    private String mThumbnail;
    private String mWebTitle;
    private String mPillarName;

    public News(String mSectionName, String mWebPublicationDate, String mWebUrl, String mHeadline, String mThumbnail, String mWebTitle, String mPillarName) {
        this.mSectionName = mSectionName;
        this.mWebPublicationDate = mWebPublicationDate;
        this.mWebUrl = mWebUrl;
        this.mHeadline = mHeadline;
        this.mThumbnail = mThumbnail;
        this.mWebTitle = mWebTitle;
        this.mPillarName = mPillarName;
    }

    public String getmSectionName() {
        return mSectionName;
    }

    public String getmWebPublicationDate() {
        return mWebPublicationDate;
    }

    public String getmWebUrl() {
        return mWebUrl;
    }

    public String getmHeadline() {
        return mHeadline;
    }

    public String getmThumbnail() {
        return mThumbnail;
    }

    public String getmWebTitle() {
        return mWebTitle;
    }

    public String getmPillarName() {
        return mPillarName;
    }

    @Override
    public String toString() {
        return "News{" +
                "mSectionName='" + mSectionName + '\'' +
                ", mWebPublicationDate='" + mWebPublicationDate + '\'' +
                ", mWebUrl='" + mWebUrl + '\'' +
                ", mHeadline='" + mHeadline + '\'' +
                ", mThumbnail='" + mThumbnail + '\'' +
                ", mWebTitle='" + mWebTitle + '\'' +
                ", mPillarName='" + mPillarName + '\'' +
                '}';
    }
}
