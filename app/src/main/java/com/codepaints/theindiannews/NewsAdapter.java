package com.codepaints.theindiannews;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

public class NewsAdapter extends RecyclerView.Adapter<NewsAdapter.NewsViewHolder> {

    private List<News> mNews;
    private Context    mContext;

    public NewsAdapter(List<News> mNews, Context mContext) {
        this.mNews = mNews;
        this.mContext = mContext;
    }


    @NonNull
    @Override
    public NewsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.news_list_item, parent, false);
        return new NewsViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull NewsViewHolder holder, int position) {

        final News news = mNews.get(position);

        holder.itemSection.setText(news.getmSectionName());
        holder.itemDate.setText(formatDate(news.getmWebPublicationDate()));
        holder.itemHeadline.setText(news.getmHeadline());
        Picasso.get().load(news.getmThumbnail()).placeholder(R.drawable.placeholder_image).into(holder.itemThumbnail);
        holder.itemContributor.setText(String.format(mContext.getResources().getString(R.string.contributor_text), news.getmPillarName(), !news.getmWebTitle().isEmpty() ? "by " + news.getmWebTitle() : ""));

        holder.itemLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Toast.makeText(mContext, news.getmWebUrl(), Toast.LENGTH_SHORT).show();
                Uri    newsUri       = Uri.parse(news.getmWebUrl());
                Intent websiteIntent = new Intent(Intent.ACTION_VIEW, newsUri);
                view.getContext().startActivity(websiteIntent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mNews.size();
    }

    public String formatDate(String parsedDate) {
//        String           initialStringDate = "2019-01-27T09:27:37Z";
        Locale           US             = new Locale("US");
        SimpleDateFormat format         = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'", US);
        String           stringDateTime = "";

        try {
            Date date = format.parse(parsedDate);

            String stringDate = new SimpleDateFormat("EEE, MMM d, ''yy", US).format(date);
            String stringTime = new SimpleDateFormat("HH:mm", US).format(date);

            stringDateTime = stringDate.concat(" at ").concat(stringTime);

            Log.i("Date_or_Time", "" + stringDateTime);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return stringDateTime;
    }

    /**
     * ViewHolder Class
     */
    public class NewsViewHolder extends RecyclerView.ViewHolder {

        public TextView         itemSection;
        public TextView         itemDate;
        public TextView         itemHeadline;
        public ImageView        itemThumbnail;
        public TextView         itemContributor;
        public ConstraintLayout itemLayout;

        public NewsViewHolder(@NonNull View itemView) {
            super(itemView);

            itemSection = itemView.findViewById(R.id.news_item_section);
            itemDate = itemView.findViewById(R.id.news_item_date);
            itemHeadline = itemView.findViewById(R.id.news_item_headline);
            itemThumbnail = itemView.findViewById(R.id.news_item_thumbnail);
            itemContributor = itemView.findViewById(R.id.news_item_contributor);
            itemLayout = itemView.findViewById(R.id.list_item_layout);

        }
    }

}
