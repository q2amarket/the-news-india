package com.codepaints.theindiannews;

import android.text.TextUtils;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

public class QueryUtils {

    public static final String LOG_TAG = QueryUtils.class.getSimpleName();

    private QueryUtils() {
    }

    public static ArrayList<News> extractNewsFromJson(String newsJSON) {

        if (TextUtils.isEmpty(newsJSON)) {
            return null;
        }

        ArrayList<News> newsArrayList = new ArrayList<>();

        try {

            JSONObject jsonObject = new JSONObject(newsJSON);
            JSONObject response   = jsonObject.getJSONObject("response");
            JSONArray  results    = response.getJSONArray("results");

            for (int r = 0; r < results.length(); r++) {

                JSONObject currentNews = results.getJSONObject(r);
                JSONObject fields      = currentNews.getJSONObject("fields");
                JSONArray  tags        = currentNews.getJSONArray("tags");


                String section         = currentNews.getString("sectionName");
                String publicationDate = currentNews.getString("webPublicationDate"); //@TODO: chang it to the date format
                String webUrl          = currentNews.getString("webUrl");
                String pillarName      = currentNews.getString("pillarName");
                String headline        = fields.getString("headline");
                String thumbnail       = fields.getString("thumbnail");
                String webTitle;

//                Log.d(LOG_TAG, "The webURL is " + webUrl);

                if (tags.length() > 0) {
                    //Log.d(LOG_TAG, "Total " + tags.length() + " items in the tags");
                    JSONObject contributorItems = tags.getJSONObject(0);
                    webTitle        = (contributorItems.has("webTitle")) ? contributorItems.getString("webTitle") : "";
//                    Log.d(LOG_TAG, "Web Title = " + webTitle + " for " + r);
                } else {
                    webTitle = "";
//                    Log.d(LOG_TAG, "Web Title = " + webTitle + " is not exists for " + r);
                }

                News news = new News(
                        section,
                        publicationDate,
                        webUrl,
                        headline,
                        thumbnail,
                        webTitle,
                        pillarName
                );

                newsArrayList.add(news);

            }


        } catch (JSONException e) {

            Log.e(LOG_TAG, "Problem parsing the news JSON results", e);

        }

        //Log.d(LOG_TAG, "return newsArrayList " + newsArrayList.toString());
        return newsArrayList;

    }

    private static String makeHttpRequest(URL url) throws IOException {

        String jsonResponse = "";

        if (url == null) {
            return jsonResponse;
        }

        HttpURLConnection urlConnection = null;
        InputStream       inputStream   = null;

        try {

            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setReadTimeout(10000);
            urlConnection.setConnectTimeout(15000);
            urlConnection.setRequestMethod("GET");
            urlConnection.connect();

            if (urlConnection.getResponseCode() == 200) {
                inputStream = urlConnection.getInputStream();
                jsonResponse = readFromStream(inputStream);
            } else {
                Log.e(LOG_TAG, "Error response code: " + urlConnection.getResponseCode());
            }

        } catch (IOException e) {

            Log.e(LOG_TAG, "Problem retrieving the earthquake JSON results", e);

        } finally {
            if (urlConnection != null) {
                urlConnection.disconnect();
            }
            if (inputStream != null) {
                inputStream.close();
            }
        }

        return jsonResponse;

    }

    private static String readFromStream(InputStream inputStream) throws IOException {

        StringBuilder output = new StringBuilder();

        if (inputStream != null) {
            InputStreamReader inputStreamReader = new InputStreamReader(inputStream, Charset.forName("UTF-8"));
            BufferedReader    reader            = new BufferedReader(inputStreamReader);
            String            line              = reader.readLine();

            while (line != null) {
                output.append(line);
                line = reader.readLine();
            }

        }

        return output.toString();

    }

    public static List<News> fetchNewsData(String requestUrl) {
        URL url = createUrl(requestUrl);

        String jsonResponse = null;

        try {

            jsonResponse = makeHttpRequest(url);

        } catch (IOException e) {

            Log.e(LOG_TAG, "Problem making the HTTP request.", e);

        }

        List<News> newsList = extractNewsFromJson(jsonResponse);

        return newsList;
    }

    private static URL createUrl(String stringUrl) {
        URL url = null;

        try {
            url = new URL(stringUrl);
        } catch (MalformedURLException e) {
            Log.e(LOG_TAG, "Error with creating URL", e);
        }

        return url;
    }

}
